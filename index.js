var Utility = {
	connectorSize: 10
};

// Connector
function Connector(node, options) {
	if(!options) options = {};
	this.rect = new Kinetic.Rect({
		x: options.x || 0,
		y: options.y || 0,
		width: options.width || Utility.connectorSize,
		height: options.height || Utility.connectorSize,
		fill: options.fill || 'red',
		stroke: options.black || 'black',
		strokeWidth: options.strokeWidth || 2,
		draggable: options.draggable || false
	});
	this.node = node;
}

// Node
function Node(options) {
	if(!options) options = {};

	this.group = new Kinetic.Group({
		x: options.x || 0,
		y: options.y || 0,
		draggable: options.draggable || true
	});

	this.rect = new Kinetic.Rect({
		x: 0,
		y: 0,
		width: options.width || 100,
		height: options.height || 100,
		fill: options.fill || 'green',
		stroke: options.black || 'black',
		strokeWidth: options.strokeWidth || 2,
		cornerRadius: options.cornerRadius || 3
	});

	this.group.add(this.rect);
	this.inlets = [];
	this.outlets = [];
}

Node.prototype.addInlet = function() {
	var connector = new Connector(this,  {
		x: -Utility.connectorSize,
		y: 5 + this.inlets.length * (Utility.connectorSize + 4)
	});
	this.group.add(connector.rect);
	this.inlets.push(connector);
}

Node.prototype.addOutlet = function() {
	var connector = new Connector(this, {
		x: this.rect.getWidth(),
		y: 5 + this.outlets.length * (Utility.connectorSize + 4)
	});
	this.group.add(connector.rect);
	this.outlets.push(connector);
}

var stage = new Kinetic.Stage({
	container: 'container',
	width: window.innerWidth,
	height: window.innerHeight,
	draggable: false
});

var layer = new Kinetic.Layer();

var node1 = new Node();
node1.addInlet();
node1.addOutlet();
node1.addOutlet();
node1.addOutlet();
node1.addInlet();
	



layer.add(node1.group);
//layer.add(node2.group);

stage.add(layer);

window.onresize = function() {
	stage.setWidth(window.innerWidth);
	stage.setHeight(window.innerHeight);
	stage.scaleX(1);
	stage.scaleY(1);
	stage.draw();
}

